MODULE AnalizadorSintactico;
  FROM Storage IMPORT ALLOCATE, DEALLOCATE;
FROM IO IMPORT RdStr, WrStr, WrLn, WrInt, WrChar;
FROM Str IMPORT  Compare, IntToStr, Concat;
IMPORT Strings;
FROM CharClass IMPORT IsLetter, IsNumeric, IsWhiteSpace;
IMPORT FIO;

TYPE  
  str= ARRAY [1..255] OF CHAR;
  ptrPila=POINTER TO nPila;
  ptrVAR=POINTER TO nVAR;
  ptrRes=POINTER TO nReser;
  
  nVAR=RECORD
    id:str;
    sig:ptrVAR;
    abajo:ptrVAR;
  END;
  
  nPila= RECORD
    id:str;
    terminal:BOOLEAN;
    abajo:ptrPila;
  END;
  
  nReser=RECORD
    id:str;
    sig:ptrRes
  END;
  
VAR    
  txt, aux, archivofinal: str;
  controltxt, af: FIO.File;
  contline, z, x:INTEGER;
  buffer, analisis:str;
  gVAR, gFOR,gWHRP,gIF, cont:ptrVAR;
  RESERVADAS:ptrRes;
  PILA:ptrPila;
  TABLAS:ptrRes;
  controlln, linealectura:str;
  contadorlinea:INTEGER;
  

PROCEDURE Delete(VAR c:str);
VAR
  x:INTEGER;
BEGIN
  FOR x := 1 TO 255 DO 
    c[x]:="" ;
  END; 
END Delete;

PROCEDURE ImprimirTabla(tabla:ptrVAR);
VAR
  temp:ptrVAR;
  aba:ptrVAR;
BEGIN
  WHILE tabla#NIL DO
    WrStr(tabla^.id);
    temp:=tabla^.sig;
    WrLn();
    WHILE temp#NIL DO
      WrStr(" ");
      WrStr(temp^.id);
      WrLn();
      IF temp^.abajo#NIL THEN
        aba:=temp^.abajo;
        WHILE aba#NIL DO
          WrStr("    ");
          WrStr(aba^.id);
          WrLn();
          aba:=aba^.abajo;
        END;
      END;
      WrLn();
      temp:=temp^.sig;
    END;
    WrLn();
    tabla:=tabla^.abajo;
  END;
END ImprimirTabla;

PROCEDURE InsertaAbajo(VAR lista:ptrVAR; dato:str);
VAR nodo:ptrVAR;
BEGIN
  IF lista=NIL THEN
    NEW(nodo);
    nodo^.id:=dato;
    nodo^.abajo:=NIL;
    nodo^.sig:=NIL;
    lista:=nodo;   
  ELSE
    InsertaAbajo(lista^.abajo, dato);  
  END;
END InsertaAbajo;

PROCEDURE InsertarLista(VAR lista:ptrVAR; dato:str);
VAR nodo:ptrVAR;
BEGIN
  IF lista=NIL THEN
    NEW(nodo);
    nodo^.id:=dato;
    nodo^.sig:=NIL;
    lista:=nodo;   
  ELSE
    InsertarLista(lista^.sig, dato);  
  END;
END InsertarLista;

PROCEDURE InsertarReservada(VAR lista:ptrRes; dato:str);
VAR nodo:ptrRes;
BEGIN
  IF lista=NIL THEN
    NEW(nodo);
    nodo^.id:=dato;
    nodo^.sig:=NIL;
    lista:=nodo;   
  ELSE
    InsertarReservada(lista^.sig, dato);  
  END;
END InsertarReservada;

PROCEDURE InsertaPila (VAR pila:ptrPila; dato:str; term:BOOLEAN);
VAR
  nodo:ptrPila;
BEGIN
  IF pila=NIL THEN
    NEW(nodo);
    nodo^.id:=dato;
    nodo^.terminal:=term;
    nodo^.abajo:=NIL;
    pila:=nodo;
  ELSE
    NEW(nodo);
    nodo^.id:=dato;
    nodo^.terminal:=term;
    nodo^.abajo:=pila;
    pila:=nodo;
  END;
END InsertaPila;

PROCEDURE AsignarCont(VAR lista:ptrVAR; VAR sig:ptrVAR);
VAR nodo:ptrVAR;                                  
BEGIN
nodo:=lista;
  WHILE nodo <> NIL DO
    IF nodo^.abajo =NIL THEN
      nodo^.sig:=sig;
      nodo:=nodo^.abajo;
      sig:=NIL;
    ELSE
      nodo:=nodo^.abajo;    
    END;
  END;
END AsignarCont;

PROCEDURE Reservadas ();
VAR
  archivo:FIO.File;
  linea:str;
BEGIN 
  archivo:=FIO.Open("reservadas.txt");
  REPEAT
    FIO.RdStr(archivo, linea);
    InsertarReservada(RESERVADAS, linea);
  UNTIL FIO.EOF;
  
  archivo:=FIO.Open("tabindex.txt");
  REPEAT
    FIO.RdStr(archivo, linea);
    InsertarReservada(TABLAS, linea);
  UNTIL FIO.EOF;
END Reservadas;

PROCEDURE BusquedaTabla (VAR lista:ptrVAR; buscar:str; datonuevo:str);(*busca en los no terminales para encontrar donde meter la interseccion*)
VAR
  temp:ptrVAR;
BEGIN
  temp:=lista;
  WHILE temp^.abajo#NIL DO
    temp:=temp^.abajo;
  END;
  temp:=temp^.sig;
  WHILE temp#NIL DO
    IF Compare(temp^.id,buscar)=0 THEN
      InsertaAbajo(temp^.abajo, datonuevo);
    END;
    temp:=temp^.sig;
  END;
END BusquedaTabla;

PROCEDURE GramaticaVAR (nombre:str; VAR lista:ptrVAR);
VAR
  archivo:FIO.File;
  aux,aux2,linea:str;
  x,y:INTEGER;
BEGIN  
  archivo:=FIO.Open(nombre);
  REPEAT
    FIO.RdStr(archivo, linea);
    x:=1;
    y:=1;
    IF linea[x]='|' THEN
      INC(x);
      WHILE (linea[x]#'{') DO
        aux[y]:=linea[x];
        INC(x);
        INC(y);
      END;
      InsertaAbajo(lista, aux);
      Delete(aux);
      INC(x);
      WHILE linea[x]#'}' DO
        y:=1;
        WHILE linea[x]#' ' DO
          aux[y]:=linea[x];
          INC(y);
          INC(x);
        END;
        InsertarLista(cont, aux);
        Delete(aux);
        INC(x);
      END;
      AsignarCont(lista, cont);
    ELSE
      WHILE (linea[x]#'{') DO
        aux[y]:=linea[x];
        INC(x);
        INC(y);
      END;  
      INC(x);
      WHILE linea[x]#'}' DO
        y:=1;
        WHILE linea[x]#' ' DO
          aux2[y]:=linea[x];
          INC(y);
          INC(x);
        END;
        BusquedaTabla(lista, aux, aux2); 
        Delete(aux2);
        INC(x);  
      END;    
    END;
    Delete(aux);
    Delete(linea);
  UNTIL FIO.EOF;
END GramaticaVAR;

PROCEDURE ObtieneBloque (archivo:FIO.File);
VAR 
  linea, final, aux, aux2:str; 
  x, y, a, end:INTEGER;
  detectaR, check:BOOLEAN;
  temp:ptrRes;
BEGIN
  x:=1;
  detectaR:=FALSE;
  check:=TRUE;  
  FIO.RdStr(archivo,linea);
  INC(contline);
  a:=1;
  y:=0;
  Delete(aux);
  Delete(aux2);
  Delete(buffer); 
  Delete(final);
  REPEAT     
    INC(y);   
    IF (IsLetter(linea[y])=TRUE)THEN
      aux[a]:=linea[y];
      INC(a);
    ELSE
      IF IsNumeric(linea[y])=TRUE THEN
        aux[a]:=linea[y];
        INC(a);
      ELSE
          temp:=RESERVADAS;
          WHILE temp#NIL DO
            IF Compare(temp^.id,aux)=0 THEN
              detectaR:=TRUE;
            END;
            temp:=temp^.sig;
          END;
          IF (detectaR=TRUE) AND (aux[1]#' ') THEN
            Concat(final, final, aux);
            Concat(final, final, " ");
          ELSE
            a:=1;
            WHILE aux[a]#'' DO
              IF IsNumeric(aux[a])=FALSE THEN
                check:=FALSE;
              END;
              INC(a);
            END;
            IF check=TRUE THEN
              Concat(final, final, "NUM ");
            ELSE
              Concat(final, final, "ID ");
            END;
            check:=TRUE;
          END;
          a:=1; 
          Delete(aux);
          detectaR:=FALSE;
        IF (IsWhiteSpace(linea[y])=FALSE) AND (linea[y]#'') THEN
          aux2[1]:=linea[y];
          Concat(final, final, aux2);
          Concat(final, final, " ");
          Delete(aux2);
        END;      
      END;
    END;      
  UNTIL linea[y]='';
  buffer:=final;
  Delete(linea);
END ObtieneBloque;

PROCEDURE EliminarNodoPila();
VAR
  temp:ptrPila;
BEGIN
  temp:=PILA^.abajo;
  DISPOSE(PILA);
  PILA:=temp;
  temp:=NIL;
END EliminarNodoPila;

PROCEDURE ImprimirPila();
VAR
  p:ptrPila;
  x,y:INTEGER;
BEGIN
  p:=PILA;
  y:=0;
  x:=0;
  WHILE p#NIL DO
    WrStr(p^.id);
    WrStr(" --> ");
    p:=p^.abajo;
  END;
  WrLn();
  
END ImprimirPila;

PROCEDURE ComprobarGramatica ();
VAR
  TablasIDS:ptrRes;
  tabla,auxTabla, sigTabla, abTabla:ptrVAR;
  stack:ptrPila;
  aux, idTabla, trans, term, NOterm:str;
  x,y,z, cterm, cterm2:INTEGER;
BEGIN
  x:=1;
  y:=1;
  Delete(aux);
  WHILE buffer[x]#' ' DO
    aux[y]:=buffer[x];
    INC(x);
    INC(y);
  END;
  TablasIDS:=TABLAS;
  WHILE TablasIDS#NIL DO
    IF Compare(TablasIDS^.id, aux)=0 THEN
      idTabla:=aux;   
    END;
    TablasIDS:=TablasIDS^.sig;
  END;
  Delete(aux);
  IF Compare(idTabla, "VAR")=0 THEN
    tabla:=gVAR;
    InsertaPila(PILA, "$", TRUE);
    InsertaPila(PILA, gVAR^.id, FALSE);
  END;
  IF Compare(idTabla, "FOR")=0 THEN
    tabla:=gFOR;
    InsertaPila(PILA, "$", TRUE);
    InsertaPila(PILA, gFOR^.id, FALSE);
  END;
  IF Compare(idTabla, "WHILE")=0 THEN
     tabla:=gWHRP;
     InsertaPila(PILA, "$", TRUE);
     InsertaPila(PILA, gWHRP^.id, FALSE);
  END;
  IF Compare(idTabla, "REPEAT")=0 THEN
     tabla:=gWHRP;
     InsertaPila(PILA, "$", TRUE);
     InsertaPila(PILA, gWHRP^.id, FALSE);
  END;
  IF Compare(idTabla, "IF")=0 THEN
     tabla:=gIF;
     InsertaPila(PILA, "$", TRUE);
     InsertaPila(PILA, gIF^.id, FALSE);
  END;
  (*AGREGAR VALIDACION PARA LAS DEMAS TABLAS*)
  Delete(idTabla);
  x:=1;
  y:=1;
  WrStr(linealectura);
  WrLn();
  WrStr("=================");
  WrLn();
  WrLn();
  Delete(linealectura);
  WHILE buffer[x]#'' DO
    y:=1;     
    WHILE buffer[x]#' ' DO
      aux[y]:=buffer[x];
      INC(y);
      INC(x);
    END;  
    IF PILA^.terminal=TRUE THEN
      IF Compare(PILA^.id, aux)=0 THEN
        EliminarNodoPila();
        REPEAT
          INC(x);
        UNTIL buffer[x]#' '; 
      END;
      Delete(aux);
    ELSE
      term:=aux;
      NOterm:=PILA^.id;
      auxTabla:=tabla;
      
      (*buscando no terminales en la tabla*)      
      WHILE auxTabla#NIL DO
        IF Compare(NOterm, auxTabla^.id)=0 THEN
          Delete(NOterm);
          sigTabla:=auxTabla^.sig;
          
          (*buscando terminal en la tabla*)
          WHILE sigTabla#NIL DO
            IF Compare(term, sigTabla^.id)=0 THEN 
              Delete(term); 
               
              (*reemplazando transicion en la pila*)
              EliminarNodoPila();
              abTabla:=sigTabla^.abajo;
              WHILE abTabla#NIL DO
                
                (*si en la transicion es un vacio*)
                IF (abTabla^.id[1]='@')THEN
                ELSE                  
                  (*si en la transicion es un terminal*)
                  IF abTabla^.id[1]="'" THEN
                    cterm:=2;
                    cterm2:=1;
                    Delete(trans);
                    WHILE abTabla^.id[cterm]#'' DO
                      trans[cterm2]:=abTabla^.id[cterm];
                      INC(cterm);
                      INC(cterm2);  
                    END;
                    InsertaPila(PILA, trans, TRUE);
                  ELSE
                    InsertaPila(PILA, abTabla^.id, FALSE);(*la transicion no es terminal*)
                  END;
                END;
                abTabla:=abTabla^.abajo;
              END;
            END;
            sigTabla:=sigTabla^.sig;
          END;
        END;
        auxTabla:=auxTabla^.abajo;
      END;
    END;
   ImprimirPila();
  END;
  IF PILA=NIL THEN
    WrStr("CADENA VALIDA");
    WrLn();
    WrLn();
    WrLn();
  END;
END ComprobarGramatica;

PROCEDURE Analisis();
VAR
  aux, linea, espacio:str;
  x,y:INTEGER;
  archivo:FIO.File;
  controlt:FIO.File;
  temp:ptrRes;
  tablaid:str;
  Encontrar:BOOLEAN;
  borrado:INTEGER;
BEGIN
  archivo:=FIO.Open("code.txt");  
  controlt:=FIO.Open(txt);
  LOOP
    FIO.RdStr(archivo,linea);
    
    INC(contadorlinea);
    IF FIO.EOF THEN
      EXIT;
    END;
    x:=1;
    Delete(aux);
    FIO.RdStr(controlt, controlln);
    REPEAT
      y:=1;      
      WHILE linea[x]#' ' DO
        aux[y]:=linea[x];
        INC(x);
        INC(y);
      END;
      temp:=TABLAS;
      WHILE temp#NIL DO
        IF Compare(temp^.id, aux)=0 THEN
          IF Encontrar=TRUE THEN
            Concat(buffer, buffer, '$ ');
            Concat(linealectura, linealectura, "$");            
            ComprobarGramatica();
            Delete(linealectura);
          END;
          Encontrar:=TRUE;
          Delete(buffer);
        END;
        temp:=temp^.sig;
      END;
      Concat(buffer, buffer, aux);
      Concat(buffer, buffer, ' ');
      Delete(aux);
      INC(x);      
    UNTIL (linea[x]='');
    Concat(linealectura, linealectura, controlln);
    Concat(linealectura, linealectura, " ");
    Delete(controlln);
  END;
  FIO.Close(archivo);
  Concat(buffer, buffer, '$ ');
  Concat(linealectura, linealectura, "$");
  ComprobarGramatica();
  Delete(linealectura);
END Analisis;

BEGIN
  WrStr("Ingrese ruta y nombre del archivo:");
  RdStr(txt);
  WrLn();
  controltxt:= FIO.Open(txt); 
  GramaticaVAR("VAR.txt", gVAR);
  GramaticaVAR("FOR.txt", gFOR);
  GramaticaVAR("WHRP.txt", gWHRP);
  GramaticaVAR("IF.txt", gIF);
  Reservadas();
  archivofinal:="code.txt";
  af:=FIO.Create(archivofinal);
  REPEAT
    ObtieneBloque(controltxt);
    FIO.WrStr(af, buffer);
    FIO.WrLn(af);
  UNTIL FIO.EOF;
  FIO.Close(af);
  FIO.Close(controltxt);
  Analisis();
END AnalizadorSintactico.